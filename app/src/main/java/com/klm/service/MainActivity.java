package com.klm.service;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_LINK = "KEY_LINK";
    private EditText edtLink;
    private TextView tvPro;
    private ProgressBar progressBar;
    private ImageView ivPhoto;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, 1);

        initView();
    }

    private void initView() {
        edtLink = findViewById(R.id.edt_link);
        ivPhoto = findViewById(R.id.iv_photo);
        tvPro = findViewById(R.id.tv_pro);
        progressBar = findViewById(R.id.pro_download);
        findViewById(R.id.bt_download).setOnClickListener(this);

        //Tạo tổng đài
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                doReceiver(intent);
            }
        };

        //Register receiver to handle action
        IntentFilter filter = new IntentFilter();
        filter.addAction(DownloadService.ACTION_SHOW_IMAGE);
        filter.addAction(DownloadService.ACTION_UPDATE_PROGRESS);

        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        //Hủy đăng kí
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private void doReceiver(Intent intent) {
        if (intent == null) return;
        if (intent.getAction() == null) return;
        if (intent.getAction().equals(DownloadService.ACTION_SHOW_IMAGE)) {
            String path = intent.getStringExtra(KEY_LINK);
            Bitmap bitmap = BitmapFactory.decodeFile(path);

            ivPhoto.setImageBitmap(bitmap);

        } else if (intent.getAction().equals(DownloadService.ACTION_UPDATE_PROGRESS)) {
            int progress = intent.getIntExtra(DownloadService.KEY_PROGRESS, 0);
            Log.i("PROGREE", progress + "");
            tvPro.setText(progress + "");
            progressBar.setProgress(progress);
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_download) {
            downLoad();
        }
    }

    private void downLoad() {
        if (edtLink.getText().toString().isEmpty()) {
            Toast.makeText(this, "Link is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        //Start 1 service để thực hiện download
        Intent intent = new Intent();
        intent.setClass(this, DownloadService.class);
        intent.putExtra(KEY_LINK, edtLink.getText().toString());
        startService(intent);
    }
}
